### Main Learnings

- Expose microfront to anothers
R: in *webpack.config.js*, inside *plugins > expose* and add the path of file that you like to share.
```
  exposes: {
    "./Counter": "./src/Counter.jsx",
    "./counterWrapper": "./src/counterWrapper.jsx",
  },
``` 
In this case, the microfrontend **remote** expose 2 components, the *Counter* and the *counterWrapper*.

- Consuming another microfrontends
R: to consume you need to know the **name**, the exposed **filename** and the exposed **url** of microfrontend. The final exposed path was like "[**name**]@$[**url**]/[**filename**]"
  Example:
  - Microfront **remote*** webpack.config.js* on ``remote@http://localhost:3000/remoteEntry.js`` expose it content for any other consume.
  ```
    output: {
      publicPath: "http://localhost:3000/", // exposed url
    },
    .
    .
    .
    plugins: [
      new ModuleFederationPlugin({
        name: "remote",
        filename: "remoteEntry.js",
        remotes: {},
        exposes: {
          "./Counter": "./src/Counter.jsx",
          "./counterWrapper": "./src/counterWrapper.jsx",
      }, ...)
    ]
  ```
  - Microfrontend **react-host**, need to register **remote** inside *webpack.config.js*
  ```
    plugins: [
      new ModuleFederationPlugin({
        name: "react_host",
        filename: "remoteEntry.js",
        remotes: {
          remote: "remote@http://localhost:3000/remoteEntry.js",
      }, ...)
    ]
  ```
- Import a component from microservice ```import ComponentX from [microserviceName]/[componentName]```. 
For example:
```
import counterWrapper from "remote/counterWrapper";
```

- **TRICK** to deal with any kind of microfrontend inside react
R: wrap a microfrontend using a ref to reference it
```
import counterWrapper from "remote/counterWrapper";

const App = () => {
  const divRef = useRef(null);

  useEffect(() => {
    counterWrapper(divRef.current);
  }, []);

  return (
    <div className="mt-10 text-3xl mx-auto max-w-6xl">
      <div>Name: react-host</div>
      <div ref={divRef}></div>
    </div>
  );
};
```


### Main Commands

- Create a microfrontend using **Module Federation** ```npx create-mf-app```

### Video Summary

video reference [link](https://www.youtube.com/watch?v=s_Fs4AXsTnA)
repository [link](https://github.com/jherr/micro-fes-in-10-minutes)



00:00 Introduction

00:13 Creating the host app

01:00 Creating the remote app

02:10 Creating the Micro-Frontend

04:05 Exposing the Micro-Frontend

04:45 Consuming the Micro-Frontend

07:00 Creating the React host app

08:00 Wrapping the Micro-Frontend

09:00 Consuming the Micro-Frontend in React

10:18 Outroduction